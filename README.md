#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>



class Triangle {
public:
	void triangle() {
		std::cout << "I am a triangle\n";
	}
};
class Equal :public Triangle
{
public:
	void equal() {
		std::cout << "In an isosceles triangle two sides are equal\n";
	}
};
class Isosceles : public Equal {
public:
	void isosceles() {
		std::cout << "I am an isosceles triangle\n";
	}
};

int main() {
	Isosceles isc;
    isc.isosceles();
	isc.equal();
	isc.triangle();
	return 0;
}

